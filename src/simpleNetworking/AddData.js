import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  ActivityIndicator,
  Modal,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {BASE_URL, TOKEN} from './Url';
import Axios from 'axios';

const AddData = ({navigation, route}, mode) => {
  const [namaMobil, setNamaMobil] = useState('');
  const [totalKM, setTotalKM] = useState('');
  const [hargaMobil, setHargaMobil] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  var dataMobil = route.params;

  useEffect(() => {
    if (route.params) {
      const data = route.params;
      setNamaMobil(data.title);
      setHargaMobil(data.harga);
      setTotalKM(data.totalKM);
    }
  }, []);

  const postData = async () => {
    //Tidak Dipakai pada halaman add data, dirubah menjadi modal tersendiri
  };

  const editData = async () => {
    setIsLoading(true);
    const body = [
      {
        _uuid: dataMobil._uuid,
        title: namaMobil,
        harga: hargaMobil,
        totalKM: totalKM,
        unitImage:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU',
      },
    ];

    const options = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: TOKEN,
      },
    };

    Axios.put(`${BASE_URL}mobil`, body, options)
      .then(response => {
        console.log('delete', response);
        setIsLoading(false);
        if (response.status === 200 || response.status === 201) {
          alert('Data Mobil berhasil diedit');
        } else {
          alert('Gagal Edit');
        }
        navigation.navigate('Home');
      })
      .catch(e => {
        console.log('error edit mobil : ', e);
      });
  };

  const deleteData = async () => {
    setIsLoading(true);
    const body = [
      {
        _uuid: dataMobil._uuid,
      },
    ];

    const options = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: TOKEN,
      },
    };

    Axios.delete(`${BASE_URL}mobil`, {data: body, ...options})
      .then(response => {
        console.log('delete', response);
        setIsLoading(false);
        if (response.status === 200 || response.status === 201) {
          alert('Data Mobil berhasil dihapus');
        } else {
          alert('Gagal Hapus');
        }
        navigation.navigate('Home');
      })
      .catch(e => {
        console.log('error delete mobil : ', e);
      });
  };

  const disableBtn = () => {
    return (
      namaMobil === '' ||
      hargaMobil === '' ||
      totalKM === '' ||
      hargaMobil < '100000000'
    );
  };

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <View
        style={{
          width: '100%',
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={{
            width: '10%',
            justifyContent: 'center',
            alignItems: 'center',
            paddingVertical: 10,
          }}>
          <Icon name="arrow-left" size={20} color="#000" />
        </TouchableOpacity>
        <Text style={{fontSize: 16, fontWeight: 'bold', color: '#000'}}>
          {dataMobil ? 'Ubah Data' : 'Tambah Data'}
        </Text>
      </View>
      <View
        style={{
          width: '100%',
          padding: 15,
        }}>
        <View>
          <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
            Nama Mobil
          </Text>
          <TextInput
            placeholder="Masukkan Nama Mobil"
            value={namaMobil}
            onChangeText={text => setNamaMobil(text)}
            style={styles.txtInput}
          />
        </View>
        <View style={{marginTop: 20}}>
          <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
            Total Kilometer
          </Text>
          <TextInput
            value={totalKM}
            onChangeText={text => setTotalKM(text)}
            placeholder="contoh: 100 KM"
            style={styles.txtInput}
          />
        </View>
        <View style={{marginTop: 20}}>
          <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
            Harga Mobil
          </Text>
          <TextInput
            value={hargaMobil}
            onChangeText={text => setHargaMobil(text)}
            placeholder="Masukkan Harga Mobil"
            style={styles.txtInput}
            keyboardType="number-pad"
          />
        </View>
        <TouchableOpacity
          disabled={disableBtn()}
          onPress={dataMobil ? editData : postData}
          style={[
            styles.btnAdd,
            disableBtn() ? {backgroundColor: '#A5A5A5'} : {},
          ]}>
          <Text style={{color: '#fff', fontWeight: '600'}}>
            {dataMobil ? 'Ubah Data' : 'Tambah Data'}
          </Text>
        </TouchableOpacity>
        {dataMobil ? (
          <TouchableOpacity
            onPress={deleteData}
            style={[styles.btnAdd, {backgroundColor: 'red'}]}>
            <Text style={{color: '#fff', fontWeight: '600'}}>Hapus Data</Text>
          </TouchableOpacity>
        ) : null}
        <Modal visible={isLoading} transparent={true}>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: 'rgba(0,0,0,0.4)',
            }}>
            <View
              style={{
                backgroundColor: 'white',
                padding: 10,
                borderRadius: 10,
              }}>
              <ActivityIndicator size="large" />
              <Text style={{alignSelf: 'center', marginTop: 10}}>Loading</Text>
            </View>
          </View>
        </Modal>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  btnAdd: {
    marginTop: 20,
    width: '100%',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: '#689f38',
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtInput: {
    marginTop: 10,
    width: '100%',
    borderRadius: 6,
    paddingHorizontal: 10,
    borderColor: '#dedede',
    borderWidth: 1,
  },
});

export default AddData;
